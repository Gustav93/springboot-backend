package com.springboot.backend.apirest.user.service;

import com.springboot.backend.apirest.user.entity.Role;

import java.util.List;

public interface RoleService {

    List<Role> findAll();
}
