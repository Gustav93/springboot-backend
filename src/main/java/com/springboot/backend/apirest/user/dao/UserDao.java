package com.springboot.backend.apirest.user.dao;

import com.springboot.backend.apirest.user.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface UserDao extends CrudRepository<User, Long> {

    User findByUsername(String username);

    @Query(value = "select u from User u where u.username=?1")
    User findByUsernameByQuery(String username);
}
