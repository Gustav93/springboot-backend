package com.springboot.backend.apirest.user.controller;


import com.springboot.backend.apirest.client.entity.Client;
import com.springboot.backend.apirest.user.entity.User;
import com.springboot.backend.apirest.user.service.RoleService;
import com.springboot.backend.apirest.user.service.UserService;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@CrossOrigin(origins = {"*"})
@RestController
@RequestMapping("/api/users")
public class UserRestController {

    private final UserService userService;

    private final RoleService roleService;

    public UserRestController(UserService userService, RoleService roleService) {
        this.userService = userService;
        this.roleService = roleService;
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @GetMapping("/list")
    public ResponseEntity<?> findAllUsers(){
        return new ResponseEntity<>(userService.findAll(), HttpStatus.OK);
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @GetMapping("/{id}")
    public ResponseEntity<?> findUserById(@PathVariable Long id){
        Map<String, Object> response = new HashMap<>();
        User user;
        try {
            user = userService.findById(id);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al realizar la consulta a la base de datos");
            response.put("error", e.getMostSpecificCause().getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if(user ==null){
            response.put("mensaje", "El usuario de id: "+ id +" no existe en la base de datos.");
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @Secured({"ROLE_ADMIN"})
    @PostMapping("/save")
    public ResponseEntity<?> saveUser(@Valid @RequestBody User user, BindingResult bindingResult){
        Map<String, Object> response = new HashMap<>();
        User newUser;

        if(bindingResult.hasErrors()){
            List<String> errors = bindingResult.getFieldErrors()
                    .stream()
                    .map(error -> "El campo ".concat(error.getField()).concat(Objects.requireNonNull(error.getDefaultMessage())))
                    .collect(Collectors.toList());
            response.put("errors", errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        try {
            newUser = userService.save(user);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al realizar el insert a la base de datos");
            response.put("error", e.getMostSpecificCause().getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(newUser, HttpStatus.CREATED);
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @PutMapping("/{id}")
    public ResponseEntity<?> updateUser(@Valid @RequestBody User user, @PathVariable Long id, BindingResult bindingResult){
        Map<String, Object> response = new HashMap<>();
        User userSource;
        User userUpdated;
        if(bindingResult.hasErrors()){
            List<String> errors = bindingResult.getFieldErrors()
                    .stream()
                    .map(error -> "El campo ".concat(error.getField()).concat(Objects.requireNonNull(error.getDefaultMessage())))
                    .collect(Collectors.toList());
            response.put("errors", errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        try {
            userSource = userService.findById(id);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al realizar el insert a la base de datos");
            response.put("error", e.getMostSpecificCause().getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if(userSource ==null){
            response.put("mensaje", "No se pudo editar. El user de id: "+ id +" no existe en la base de datos.");
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }

        try {
            userSource.setName(user.getName());
            userSource.setSurname(user.getSurname());
            userSource.setEmail(user.getEmail());
            userSource.setPassword(user.getPassword());
            userSource.setRoles(user.getRoles());
            userUpdated = userService.save(userSource);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al realizar el update a la base de datos");
            response.put("error", e.getMostSpecificCause().getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(userUpdated, HttpStatus.CREATED);
    }

    @Secured({"ROLE_ADMIN"})
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteClient(@PathVariable Long id){
        Map<String, Object> response = new HashMap<>();
        try {
            userService.delete(id);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al eliminar el cliente la base de datos");
            response.put("error", e.getMostSpecificCause().getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @Secured({"ROLE_ADMIN"})
    @GetMapping("/roles")
    public ResponseEntity<?> findAllRoles(){
        return new ResponseEntity<>(roleService.findAll(), HttpStatus.OK);
    }
}
