package com.springboot.backend.apirest.user.dao;

import com.springboot.backend.apirest.user.entity.Role;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RoleDao extends CrudRepository<Role, Long> {

    List<Role> findAll();
}
