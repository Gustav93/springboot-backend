package com.springboot.backend.apirest.user.service;

import com.springboot.backend.apirest.user.entity.User;

import java.util.List;

public interface UserService {

    User findByUsername(String username);

    User findById(Long id);

    List<User> findAll();

    User save(User user);

    void delete(Long id);
}
