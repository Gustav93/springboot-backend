package com.springboot.backend.apirest.client.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;

public interface UploadImageService {

    Resource load(String imageName) throws MalformedURLException;

    String save(MultipartFile multipartFile) throws IOException;

    boolean delete(String imageName);

    Path getPath(String imageName);
}
