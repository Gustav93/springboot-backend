package com.springboot.backend.apirest.client.dao;

import com.springboot.backend.apirest.client.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientDao extends JpaRepository<Client, Long> {
}
