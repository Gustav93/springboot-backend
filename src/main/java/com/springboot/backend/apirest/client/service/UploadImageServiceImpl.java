package com.springboot.backend.apirest.client.service;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.UUID;

@Service
public class UploadImageServiceImpl implements UploadImageService {

    private static final String UPLOAD_DIR = "uploads";

    @Override
    public Resource load(String imageName) throws MalformedURLException {
        Path imagePath = getPath(imageName);
        Resource resource = new UrlResource(imagePath.toUri());

        if(!resource.exists() || !resource.isReadable()){
            throw new RuntimeException("No se pudo cargar la imagen " + imageName);
        }
        return resource;
    }

    @Override
    public String save(MultipartFile multipartFile) throws IOException {
        String fileName = "";
        if(!multipartFile.isEmpty()){
            fileName = UUID.randomUUID().toString()
                    .concat("_")
                    .concat(Objects.requireNonNull(multipartFile.getOriginalFilename()))
                    .replace(" ","-");
            Path pathFolder = Paths.get("uploads").resolve(fileName).toAbsolutePath();
            Files.copy(multipartFile.getInputStream(), pathFolder);
        }
        return fileName;
    }

    @Override
    public boolean delete(String imageName) {
        if(imageName!=null && !imageName.isEmpty()){
            Path oldImagePath = Paths.get("uploads").resolve(imageName).toAbsolutePath();
            File oldImage = oldImagePath.toFile();
            if(oldImage.exists() && oldImage.canRead()){
                 return oldImage.delete();
            }
        }
        return false;
    }

    @Override
    public Path getPath(String imageName) {
        return Paths.get(UPLOAD_DIR).resolve(imageName).toAbsolutePath();
    }
}
