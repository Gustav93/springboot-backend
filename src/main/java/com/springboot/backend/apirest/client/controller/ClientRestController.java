package com.springboot.backend.apirest.client.controller;

import com.springboot.backend.apirest.client.entity.Client;
import com.springboot.backend.apirest.client.service.UploadImageService;
import com.springboot.backend.apirest.client.service.ClientService;
import org.springframework.core.io.Resource;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.*;
import java.util.stream.Collectors;

@CrossOrigin(origins = {"*"})
@RestController
@RequestMapping("/api/clients")
public class ClientRestController {

    private final ClientService clientService;

    private final UploadImageService uploadImageService;

    public ClientRestController(ClientService clientService, UploadImageService uploadImageService) {
        this.clientService = clientService;
        this.uploadImageService = uploadImageService;
    }

    @GetMapping("/list")
    @ResponseStatus(HttpStatus.OK)
    public List<Client> findAllClients(){
        return clientService.findAll();
    }

    @GetMapping("/page/{page}")
    @ResponseStatus(HttpStatus.OK)
    public Page<Client> findAllClients(@PathVariable Integer page){
        return clientService.findAll(PageRequest.of(page,5));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findClientById(@PathVariable Long id){
        Map<String, Object> response = new HashMap<>();
        Client client;
        try {
            client = clientService.findById(id);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al realizar la consulta a la base de datos");
            response.put("error", e.getMostSpecificCause().getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if(client ==null){
            response.put("mensaje", "El cliente de id: "+ id +" no existe en la base de datos.");
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(client, HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<?> saveClient(@Valid @RequestBody Client client, BindingResult bindingResult){
        Map<String, Object> response = new HashMap<>();
        Client newClient;

        if(bindingResult.hasErrors()){
            List<String> errors = bindingResult.getFieldErrors()
                    .stream()
                    .map(error -> "El campo ".concat(error.getField()).concat(Objects.requireNonNull(error.getDefaultMessage())))
                    .collect(Collectors.toList());
            response.put("errors", errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        try {
            newClient = clientService.save(client);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al realizar el insert a la base de datos");
            response.put("error", e.getMostSpecificCause().getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(newClient, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateClient(@Valid @RequestBody Client client, @PathVariable Long id, BindingResult bindingResult){
        Map<String, Object> response = new HashMap<>();
        Client clientSource;
        Client clientUpdated;
        if(bindingResult.hasErrors()){
            List<String> errors = bindingResult.getFieldErrors()
                    .stream()
                    .map(error -> "El campo ".concat(error.getField()).concat(Objects.requireNonNull(error.getDefaultMessage())))
                    .collect(Collectors.toList());
            response.put("errors", errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        try {
            clientSource = clientService.findById(id);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al realizar el insert a la base de datos");
            response.put("error", e.getMostSpecificCause().getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if(clientSource ==null){
            response.put("mensaje", "No se pudo editar. El cliente de id: "+ id +" no existe en la base de datos.");
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }

        try {
            clientSource.setName(client.getName());
            clientSource.setSurname(client.getSurname());
            clientSource.setEmail(client.getEmail());
            clientSource.setPassword(client.getPassword());
            clientSource.setCreateAt(client.getCreateAt());
            clientUpdated = clientService.save(clientSource);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al realizar el update a la base de datos");
            response.put("error", e.getMostSpecificCause().getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(clientUpdated, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteClient(@PathVariable Long id){
        Map<String, Object> response = new HashMap<>();
        try {
            Client client = clientService.findById(id);
            uploadImageService.delete(client.getImage());
            clientService.delete(id);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al eliminar el cliente la base de datos");
            response.put("error", e.getMostSpecificCause().getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @PostMapping("/upload")
    public ResponseEntity<?> uploadImage(@RequestParam("file") MultipartFile file, @RequestParam("id") Long id){
        Map<String, Object> response = new HashMap<>();
        Client client = clientService.findById(id);

        if(!file.isEmpty()){
            String fileName;
            try {
                fileName = uploadImageService.save(file);
            } catch (IOException e) {
                response.put("mensaje", "Error al surbir la imagen");
                response.put("error", e.getCause().getMessage());
                return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            uploadImageService.delete(client.getImage());
            client.setImage(fileName);
            clientService.save(client);

            response.put("user", client);
            response.put("message", "La imagen ".concat(fileName).concat(" se subio correctamente"));
        }

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("/img/{imageName:.+}")
    public ResponseEntity<Resource> showImage(@PathVariable String imageName){
        Resource resource = null;
        try {
            resource = uploadImageService.load(imageName);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE,"image/*");
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename\""+resource.getFilename()+"\"");
        return new ResponseEntity<>(resource, headers, HttpStatus.OK);
    }
}
