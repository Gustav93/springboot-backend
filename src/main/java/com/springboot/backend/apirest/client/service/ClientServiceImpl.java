package com.springboot.backend.apirest.client.service;

import com.springboot.backend.apirest.client.dao.ClientDao;
import com.springboot.backend.apirest.client.entity.Client;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {

    private final ClientDao clientDao;

    public ClientServiceImpl(ClientDao clientDao) {
        this.clientDao = clientDao;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Client> findAll() {
        return (List<Client>) clientDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Client> findAll(Pageable pageable) {
        return clientDao.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Client findById(Long id) {
        return clientDao.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public Client save(Client client) {
        return clientDao.save(client);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        Client client = findById(id);
        if(client.getImage()!=null && !client.getImage().isEmpty()){
            Path oldImagePath = Paths.get("uploads").resolve(client.getImage()).toAbsolutePath();
            File oldImage = oldImagePath.toFile();
            if(oldImage.exists() && oldImage.canRead()){
                oldImage.delete();
            }
        }
        clientDao.deleteById(id);
    }
}
