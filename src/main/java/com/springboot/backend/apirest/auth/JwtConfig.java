package com.springboot.backend.apirest.auth;

public class JwtConfig {

    public static final String PUBLIC_RSA_KEY = "-----BEGIN PUBLIC KEY-----\n" +
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvoRm0JKhlKmqbLpPVUZ9\n" +
            "42FTHb8bYe0l7kMiFV0/X6Pv6v9Nb761NsslGm7cz7vfCV/ZbfJabVOGNYR9BFHj\n" +
            "6y028TobvgGXMcIY3PW2UY+04z4xbBknqX7TeEwNHGoEzti3ghuJ8SjV/aw86mr3\n" +
            "EOLHnVhik/1NTKip00pxpzsajyPCI/8jfrMS+pBJZUNApFZ2MV6niWj89k3943Xp\n" +
            "hSR0dmkC419eGdR6g0zovTDAvzgjPii9MCTLIqz3DA5Q4SgLGtiVcm6UP045qyUV\n" +
            "NuBuu6iw4LLijGj27CUljKaGFJAb/u0UCFkQ3YApAWDrE3p6QVytNlVMPcOwfhPS\n" +
            "uwIDAQAB\n" +
            "-----END PUBLIC KEY-----";

    public static final String PRIVATE_RSA_KEY = "-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIIEowIBAAKCAQEAvoRm0JKhlKmqbLpPVUZ942FTHb8bYe0l7kMiFV0/X6Pv6v9N\n" +
            "b761NsslGm7cz7vfCV/ZbfJabVOGNYR9BFHj6y028TobvgGXMcIY3PW2UY+04z4x\n" +
            "bBknqX7TeEwNHGoEzti3ghuJ8SjV/aw86mr3EOLHnVhik/1NTKip00pxpzsajyPC\n" +
            "I/8jfrMS+pBJZUNApFZ2MV6niWj89k3943XphSR0dmkC419eGdR6g0zovTDAvzgj\n" +
            "Pii9MCTLIqz3DA5Q4SgLGtiVcm6UP045qyUVNuBuu6iw4LLijGj27CUljKaGFJAb\n" +
            "/u0UCFkQ3YApAWDrE3p6QVytNlVMPcOwfhPSuwIDAQABAoIBAEgHsv+kkKauRxr4\n" +
            "jcoF7s6rG0uh1258xNyCUT6WlMFKMAk/IT3gco8W2x5VT/LSaw8WSBSDjqDK9XEC\n" +
            "eGnLtEpUO/OULqz12P4dDl1sINRRauiHuRPPpuk0ZCuVRVe61bqGwUrVsp+U+uL/\n" +
            "3cAbVPHza9kXc53UJQeYtSyHdtTsoi4cigJrrncepKKsz8LDRq5ztKZvJCJjtaer\n" +
            "ciK61te1Uf8xVCHlv/pVzVA7aVJ2Q8zrOcHSCWZCLf3CYKqQHErBTy/yUfxsNb75\n" +
            "P9nK1/agoHM1KV+Q/Q17lViQZcqCFGJjz3xi4ZoFYqJvnXTHN8JdXI4zUv0eDLAB\n" +
            "ioIENHECgYEA7oyTy6VLuI4Yzcq/idNGgItg0Er+t9RJuiMlo9nSw7xW8OQyPmAU\n" +
            "JB1kMBJZZb/zKNMSfPly+UhnZiwDM/gbnid5F8Yz2kVB27uv8ry2bWwEqRWBpp7Y\n" +
            "0Yy5COMnVICYyq1Q5E8ZU31L+WHZIEqZ0091TgejX65/AjCDY2GUGG8CgYEAzHRO\n" +
            "tNEBuSSUnGoXmVOX8F7HCl1GTsj7OTO6N6GQ3WT/FrXoIR0tFGtCXAaG2NOg645Y\n" +
            "SemWDgr0ay8SFmy7JVY2pfQb1hKZc0d6K2BNNCo8LUHq/Ge5aWRvkzz7xGmOqbsu\n" +
            "Frn9Y6F7Q0AJJTm8Z31hfNvkZQU7qpdNNekp2HUCgYBw1W+nGMWLQzM6TBdzQssL\n" +
            "TGH7xC1dmH1THQLV0NdfnDtncBjP5jRArOJR1qwQc825VrVdBcS7YuL9sf3F3Cp8\n" +
            "EWG2vBqUuMOhAtMDtv13HmvJ6pFL1cDqxWxUZtWYo8chvv8PLtbk5dRE3mKMYp0G\n" +
            "FYR/5x8AyBHtYDMVk/cv4wKBgA+kXlkrHGyjxdLpswCCCr/TIHtEg5qHeeD4eijD\n" +
            "bafikDmWlTnjV42WXy9XOH3iQw+ChnKxMk/EuspwgXvBudezbt7nfDG2npLZRxmG\n" +
            "dqGlEZGM14OUE/ViP78ODV0LjmSh83a9qAs1UY7lmU2Fy0pHp2u8Dw9DhTsZ9EhJ\n" +
            "nG/5AoGBAO0AA6lQmcKdydAjwbQvzCm2CLeJ+j8PNYbiNXzTxJfEnvDppBfrvDQq\n" +
            "Q5qgFs2eT77qUK5SzG2x7QvgtEJPy7Q50NKkwx4KgzOGuryrutlNPM3EDC8VoNgW\n" +
            "2pQAgaYWI5QW3ciqIYI/6L2Mv8iMD2ZBcrotmFQs96EmioxhWvQ7\n" +
            "-----END RSA PRIVATE KEY-----";

}
